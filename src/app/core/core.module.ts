import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CoreRoutingModule } from './core-routing.module';
import { httpInterceptorProviders } from './interceptors/auth-interceptor'
import { Adminguard } from './guards/adminguard';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    CoreRoutingModule
  ],
  exports: [
    BrowserModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [httpInterceptorProviders, Adminguard]
})
export class CoreModule { }
