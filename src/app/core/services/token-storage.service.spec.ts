import { TestBed, async, fakeAsync } from '@angular/core/testing';
import { TokenStorageService } from './token-storage.service';
describe('TokenStorageService', () => {

  // TOKEN_KEY
  const TOKEN = 'AuthToken';
  const USERNAME = 'AuthUsername';
  const AUTHORITIES = 'AuthAuthorities';
  let tokenStorage: TokenStorageService;
  beforeEach(() => TestBed.configureTestingModule({

  }));

  beforeEach(() => {
    tokenStorage = TestBed.get(TokenStorageService);
    });

  it('should be created', () => {
    expect(tokenStorage).toBeTruthy();
  });

  it('should return the same tokens from sessionStorage and tokenStorage', () => {
    tokenStorage.saveToken('SampleJWTTokenXXX');
    expect(sessionStorage.getItem(TOKEN)).toEqual('SampleJWTTokenXXX');
    console.log(sessionStorage.getItem(TOKEN));
    const token: string = tokenStorage.getToken();
    expect(token).toEqual('SampleJWTTokenXXX');
    tokenStorage.signOut();
    expect(sessionStorage.length).toEqual(0);
  });

  it('should return the same User from sessionStorage and tokenStorage', () => {
    tokenStorage.saveUsername('Inn');
    expect(sessionStorage.getItem(USERNAME)).toEqual('Inn');
    console.log(sessionStorage.getItem(USERNAME));
    const username: string = tokenStorage.getUsername();
    expect(username).toEqual('Inn');
    sessionStorage.clear();
  });

  it('should return the Authorities from sessionStorage', () => {
    const roles: string[] = [];
    roles.push('GUEST');
    const roles1: string[] = [];
    tokenStorage.saveAuthorities(roles);
    tokenStorage.saveToken('ABCTokenMock');
    console.log(sessionStorage.getItem(AUTHORITIES));
    const authorities: string[] = tokenStorage.getAuthorities();
    // expect(authorities).toEqual(['GUEST']);
    JSON.parse(sessionStorage.getItem(AUTHORITIES)).forEach(authority => {
      roles1.push(authority);
      console.log(roles1);
    });
    expect(roles1).toEqual(['GUEST']);
    sessionStorage.clear();
    console.log(tokenStorage.getToken());
  });


});
