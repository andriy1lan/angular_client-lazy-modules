import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../../../../core/services/user.service';
import { User} from '../../../../models/user.model';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../../../core/services/token-storage.service';
import { EditUserComponent } from '../edit-user/edit-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  isupdating = false;
  isdeleted = false;

  constructor(private userService: UserService, private router: Router, private token: TokenStorageService) { }
  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.userService.getUserSt(this.token.getUsername()).subscribe(data => this.user = data);
  }

  deleteUser() {
    this.isupdating = false;
    this.userService.deleteUser(this.user.id)
      .subscribe(
        data => {
          console.log(data);
          this.isdeleted = true;
        },
        error => console.log(error));
  }

  updateUser() {
    if (this.isdeleted) { return; }
    // if (localStorage.getItem('u')!=undefined) localStorage.removeItem('u');
    // localStorage.setItem('u',JSON.stringify(this.user));
    // this.router.navigate(['/update']);
    console.log(this.user);
    this.isupdating = true;
  }


}
