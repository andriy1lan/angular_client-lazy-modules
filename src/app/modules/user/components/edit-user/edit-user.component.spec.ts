import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { EditUserComponent } from './edit-user.component';
import { User } from '../../../../models/user.model';
import { Role } from '../../../../models/role.model';

describe('EditUserComponent', () => {
  let component: EditUserComponent;
  let fixture: ComponentFixture<EditUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ EditUserComponent ],
      providers: [HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditUserComponent);
    component = fixture.componentInstance;
    component.user = new User();
    // component.user.id=1; component.user.name="myname"; component.user.username="myusername";
    // component.user.email="my@email.com"; component.user.password="password";
    // mocking partially the input object in child component
    component.user.roles = [];
    component.user.roles.push(new Role());
    component.user.roles[0].id = 2;
    component.user.roles[0].role = 'ROLE_GUEST';
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});
