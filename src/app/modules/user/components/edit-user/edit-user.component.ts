import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { UserService } from '../../../../core/services/user.service';
import { User} from '../../../../models/user.model';
import { Role} from '../../../../models/role.model';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @Input() user: User;
  submitted = false;
  registerForm: FormGroup;

  constructor(private userService: UserService) { }
  ngOnInit() {

     let admin: boolean;
     let guest: boolean;
     this.user.roles.forEach(role => {if (role.role === 'ROLE_ADMIN') {admin = true; }
                                      if (role.role === 'ROLE_GUEST') {guest = true; }
     });

     this.registerForm = new FormGroup({
      id: new FormControl(this.user.id),
      name: new FormControl(this.user.name, Validators.required),
      username: new FormControl(this.user.username, [Validators.required, Validators.minLength(3)]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3)]),
      isAdmin: new FormControl(admin),
      isGuest: new FormControl(guest)
    });
     console.log(this.registerForm);
  }

  update() {
    console.log(this.registerForm);
    this.user.name = this.registerForm.value.name;
    this.user.username = this.registerForm.value.username;
    this.user.email = this.registerForm.value.email;
    this.user.password = this.registerForm.value.password;
    this.user.roles = [];
    const role1: Role = new Role(); role1.id = 7; role1.role = 'ROLE_ADMIN'; // there is checker for original id for the roles on server update method
    const role2: Role = new Role(); role2.id = 8; role2.role = 'ROLE_GUEST';
    if (this.registerForm.value.isAdmin) { this.user.roles.push(role1); }
    if (this.registerForm.value.isGuest) { this.user.roles.push(role2); }

    console.log(this.user);
    this.userService.updateUser(this.user.id, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.submitted = true;
    this.user = new User();
  }
}
