import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminpaneComponent } from './components/adminpane/adminpane.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [AdminpaneComponent],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
