import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminpaneComponent } from './components/adminpane/adminpane.component';
import { Adminguard } from '../../core/guards/adminguard';

const routes: Routes = [
  {path: '', component:AdminpaneComponent, canActivate: [Adminguard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
