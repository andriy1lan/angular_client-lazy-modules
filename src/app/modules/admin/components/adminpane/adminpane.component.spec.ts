import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RolesPipe } from '../../../../shared/pipes/roles.pipe';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AdminpaneComponent } from './adminpane.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

describe('AdminpaneComponent', () => {
  let component: AdminpaneComponent;
  let fixture: ComponentFixture<AdminpaneComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
    ],
      declarations: [ AdminpaneComponent, RolesPipe],
      providers: [HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminpaneComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
