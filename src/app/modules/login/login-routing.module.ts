import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { LoginRComponent } from './components/login-r/login-r.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterRComponent } from './components/register-r/register-r.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full' },
  //{path: '', component:HomeComponent},
  {path: 'home', component:HomeComponent},
  {path: 'login', component: LoginRComponent},
  {path: 'register', component: RegisterRComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
