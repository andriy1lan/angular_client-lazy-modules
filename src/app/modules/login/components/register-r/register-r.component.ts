import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services/auth.service';
import { SignupInfo } from '../../../../models/signup-info';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from '../../../../core/services/user.service';
import { User} from '../../../../models/user.model';
import { Role} from '../../../../models/role.model';

@Component({
  selector: 'app-register-r',
  templateUrl: './register-r.component.html',
  styleUrls: ['./register-r.component.css']
})
export class RegisterRComponent implements OnInit {

  registerForm = new FormGroup({
    name: new FormControl('', Validators.required),
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)]),
    isAdmin: new FormControl(false),
    isGuest: new FormControl(true, Validators.requiredTrue)
  });

  roles = [
    { id: 1, name: 'ROLE_ADMIN' },
    { id: 2, name: 'ROLE_GUEST' }
  ];

  user: User;
  signupInfo: SignupInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService, private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.registerForm.value);
    this.user = new User();
    this.user.name = this.registerForm.value.name;
    this.user.username = this.registerForm.value.username;
    this.user.email = this.registerForm.value.email;
    this.user.password = this.registerForm.value.password;
    this.user.roles = [];
    const role1: Role = new Role(); role1.id = 7; role1.role = 'ROLE_ADMIN'; // there is checker for original id for the roles on server update method
    const role2: Role = new Role(); role2.id = 8; role2.role = 'ROLE_GUEST';
    if (this.registerForm.value.isAdmin) { this.user.roles.push(role1); }
    if (this.registerForm.value.isGuest) { this.user.roles.push(role2); }
    console.log(this.user);

    this.userService.createUser(this.user)
      .subscribe(
        data => {
          console.log(data);
          this.isSignedUp = true;
          this.isSignUpFailed = false;
        },
        error => {
          console.log(error);
          this.errorMessage = error.error;
          this.isSignUpFailed = true;
        }
      );

    }

    anew() {
      this.isSignedUp = false;
      this.signupInfo = null;
      this.registerForm.reset();
      console.log(this.signupInfo);
      console.log(this.registerForm.value);

    }
}
