import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services//auth.service';
import { TokenStorageService } from '../../../../core/services//token-storage.service';
import { LoginInfo } from '../../../../models/login-info';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-login-r',
  templateUrl: './login-r.component.html',
  styleUrls: ['./login-r.component.css']
})
export class LoginRComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = 'Check username/password';
  errorMessage1 = 'net::ERR_CONNECTION_REFUSED';
  roles: string[] = [];
  username: string;
  private loginInfo: LoginInfo;

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
      this.username = this.tokenStorage.getUsername();
    }
  }

  onSubmit() {
    console.log(this.loginForm.value);

    this.loginInfo = new LoginInfo(
        this.loginForm.value.username,
        this.loginForm.value.password);

    this.authService.attemptAuth(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getAuthorities();
        this.redirectPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.text;
        this.isLoginFailed = true;
      }
    );
  }
  redirectPage() {
    window.location.href = '/home';
  }





}
