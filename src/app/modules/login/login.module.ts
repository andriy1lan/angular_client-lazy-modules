import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { LoginRComponent } from './components/login-r/login-r.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterRComponent } from './components/register-r/register-r.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [HomeComponent,
    LoginRComponent,
    RegisterRComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
