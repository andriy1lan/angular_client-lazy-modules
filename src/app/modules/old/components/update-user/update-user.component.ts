import { Component, OnInit } from '@angular/core';
import { User} from '../../../../models/user.model';
import { UserService } from '../../../../core/services/user.service';
import { Router } from '@angular/router';
import { Role } from '../../../../models/role.model';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  isAdmin = false;
  isGuest = false;
  user: User = new User();

  submitted = false;
  constructor(private userService: UserService, private router: Router) { }
  ngOnInit() {


        // this.user=JSON.parse(localStorage.getItem('u'));
        this.user.password = '';

    // this.user.roles=new Array<Role>(2);
    // this.user.roles[0]=new Role();
    // this.user.roles[1]=new Role();
  }
  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }
  save() {
    this.user.roles = [];
    const role1: Role = new Role(); role1.id = 7; role1.role = 'ROLE_ADMIN'; // there is checker for original id for the roles on server update method
    const role2: Role = new Role(); role2.id = 8; role2.role = 'ROLE_GUEST';
    if (this.isAdmin) { this.user.roles.push(role1); }
    if (this.isGuest) { this.user.roles.push(role2); }
    this.userService.updateUser(this.user.id, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }

  updateUser() {
    this.router.navigate(['/update']);
    window.location.reload();
  }

}
