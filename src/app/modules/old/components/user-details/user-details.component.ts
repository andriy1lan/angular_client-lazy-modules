import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../../../core/services/user.service';
import { User } from '../../../../models/user.model';
import { UserListComponent } from '../user-list/user-list.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  @Input() user: User;
  constructor(private userService: UserService, private listComponent: UserListComponent,
              private router: Router) { }
  ngOnInit() {
  }

  deleteUser() {
    this.userService.deleteUser(this.user.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }

  updateUser() {
    this.router.navigate(['/update']);
  }
}
