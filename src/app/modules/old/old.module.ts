import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { OldRoutingModule } from './old-routing.module';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';

@NgModule({
  declarations: [
    CreateUserComponent,
  UpdateUserComponent,
  UserListComponent,
  UserDetailsComponent
],
  imports: [
    CommonModule,
    SharedModule,
    OldRoutingModule
  ]
})
export class OldModule { }
